cmake_minimum_required (VERSION 3.1)

project (ffms2)

file (GLOB AVSSOURCES "src/avisynth/*.cpp")
file (GLOB VPYSOURCES "src/vapoursynth/*.cpp")
file (GLOB CORE "src/core/*.cpp")

find_package (Git)

add_library (ffms2 SHARED ${AVSSOURCES} ${VPYSOURCES} ${CORE} ${CMAKE_CURRENT_BINARY_DIR}/ffms2.rc)

if (GIT_FOUND)
    execute_process (COMMAND ${GIT_EXECUTABLE} rev-list --count main
        OUTPUT_VARIABLE ver
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    execute_process (COMMAND ${GIT_EXECUTABLE} rev-list --count main..
        OUTPUT_VARIABLE diff
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    execute_process (COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
        OUTPUT_VARIABLE hash
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    execute_process (COMMAND ${GIT_EXECUTABLE} rev-list --count HEAD
        OUTPUT_VARIABLE mod
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    math(EXPR mod "${mod} + 1000")
else ()
    message (STATUS "GIT not found")
endif ()

configure_file (
    "${CMAKE_CURRENT_SOURCE_DIR}/ffms2.rc.in"
    "${CMAKE_CURRENT_BINARY_DIR}/ffms2.rc"
)

include_directories (include
    src/avisynth
    src/vapoursynth
    src/core
    ../AviSynthPlus/avs_core/include
)

target_link_libraries (ffms2 PRIVATE bcrypt.lib)

get_filename_component (deps ../deps ABSOLUTE)

if (CMAKE_SIZEOF_VOID_P EQUAL 8)
    include_directories (../deps/x64-Release/include)
    target_link_libraries (ffms2 PUBLIC
        ${deps}/x64-Release/lib/libavutil.a
        ${deps}/x64-Release/lib/libavcodec.a
        ${deps}/x64-Release/lib/libavformat.a
        ${deps}/x64-Release/lib/libswscale.a
        ${deps}/x64-Release/lib/libswresample.a
        ${deps}/x64-Release/lib/zlib.lib
        ${deps}/x64-Release/lib/libdav1d.a
        ${deps}/x64-Release/lib/libxml2s.lib
    )
else (CMAKE_SIZEOF_VOID_P EQUAL 4)
    include_directories (../deps/Win32-Release/include)
    target_link_libraries (ffms2 PUBLIC
        ${deps}/Win32-Release/lib/libavutil.a
        ${deps}/Win32-Release/lib/libavcodec.a
        ${deps}/Win32-Release/lib/libavformat.a
        ${deps}/Win32-Release/lib/libswscale.a
        ${deps}/Win32-Release/lib/libswresample.a
        ${deps}/Win32-Release/lib/zlib.lib
        ${deps}/Win32-Release/lib/libdav1d.a
        ${deps}/Win32-Release/lib/libxml2s.lib
    )
endif ()

add_definitions (-D_CRT_SECURE_NO_WARNINGS
    -D__STDC_CONSTANT_MACROS
    -DNDEBUG
)

target_compile_definitions (ffms2 PRIVATE
    -DFFMS_EXPORTS
    -D_WINDOWS
    -D_USRDLL
    -DFFMS2_EXPORTS
    FFMS_REV=${mod}
)

project (ffmsindex)

add_executable (ffmsindex src/index/ffmsindex.cpp src/index/ffmsindex.manifest ${CMAKE_CURRENT_BINARY_DIR}/ffmsindex.rc)

configure_file (
    "${CMAKE_CURRENT_SOURCE_DIR}/ffmsindex.rc.in"
    "${CMAKE_CURRENT_BINARY_DIR}/ffmsindex.rc"
)

include_directories (src/index)

target_link_libraries (ffmsindex ffms2)

target_compile_definitions (ffmsindex PRIVATE -D_CONSOLE)
